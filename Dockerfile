FROM openjdk:8-jre-alpine

WORKDIR /appagent
COPY /appagent ./

WORKDIR /app
COPY target/movimentacao-*.jar api-movimentacao.jar
CMD ["java", "-javaagent:/appagent/javaagent.jar", "-jar", "/app/api-movimentacao.jar"]
