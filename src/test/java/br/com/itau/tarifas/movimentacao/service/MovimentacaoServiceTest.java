package br.com.itau.tarifas.movimentacao.service;

import br.com.itau.tarifas.movimentacao.DTO.MovimentacaoRequest;
import br.com.itau.tarifas.movimentacao.client.customer.Customer;
import br.com.itau.tarifas.movimentacao.client.customer.CustomerClient;
import br.com.itau.tarifas.movimentacao.client.tarifa.Tarifa;
import br.com.itau.tarifas.movimentacao.client.tarifa.TarifaClient;
import br.com.itau.tarifas.movimentacao.models.Movimentacao;
import br.com.itau.tarifas.movimentacao.models.Tipo;
import br.com.itau.tarifas.movimentacao.repository.MovimentacaoRepository;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = MovimentacaoService.class)
public class MovimentacaoServiceTest {

    @Autowired
    private MovimentacaoService movimentacaoService;

    @MockBean
    private MovimentacaoRepository movimentacaoRepository;

    @MockBean
    private CustomerClient customerClient;

    @MockBean
    private TarifaClient tarifaClient;

    private List<Movimentacao> movimentacaoList;

    private Movimentacao movimentacao;

    @Before
    public void setUp() throws Exception {
        movimentacao = mockMovimentacao();
        movimentacaoList = new ArrayList<>();
        movimentacaoList.add(movimentacao);

    }

    @Test
    public void getById() {
        when(movimentacaoRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(movimentacao));
        Movimentacao movimentacaoCriada = movimentacaoService.getById(Mockito.anyInt());

        assertEquals(movimentacao, movimentacaoCriada);
    }

    @Test
    public void pesquisarData() {
        when(movimentacaoRepository.findByDataMovimentoBetween(Mockito.any(LocalDate.class), Mockito.any(LocalDate.class)))
                .thenReturn(movimentacaoList);

        Iterable<Movimentacao> movimentacaoIterable = movimentacaoService.pesquisarData(1, 1);

        assertEquals(movimentacao, Lists.newArrayList(movimentacaoIterable).get(0));
    }

    @Test
    public void getAll(){
        when(movimentacaoRepository.findAll()).thenReturn(movimentacaoList);

        Iterable<Movimentacao> movimentacaoIterable = movimentacaoService.getAll();

        assertEquals(movimentacao, Lists.newArrayList(movimentacaoIterable).get(0));
    }

    @Test
    public void getByCpfcnpj(){
        when(movimentacaoRepository.findByCpfCnpj(Mockito.anyString())).thenReturn(movimentacaoList);
        when(customerClient.getByCpfcnpj(Mockito.anyString())).thenReturn(mockCustomer());
        Iterable<Movimentacao> movimentacaoIterable = movimentacaoService.getByCpfcnpj(Mockito.anyString());

        assertEquals(movimentacao, Lists.newArrayList(movimentacaoIterable).get(0));
    }

    @Test
    public void create(){
        when(movimentacaoRepository.save(Mockito.any(Movimentacao.class))).thenReturn(movimentacao);
        when(customerClient.getByCpfcnpj(Mockito.anyString())).thenReturn(mockCustomer());
        when(tarifaClient.getById(Mockito.anyInt())).thenReturn(mockTarifa());

        Movimentacao movimentacaoCriada = movimentacaoService.create(movimentacao);

        assertEquals(movimentacao, movimentacaoCriada);
    }

    public Movimentacao mockMovimentacao(){
        Movimentacao movimentacao = new Movimentacao();
        movimentacao.setId(1);
        movimentacao.setIdTarifa(1);
        movimentacao.setCpfCnpj("12345678910");
        movimentacao.setValorMovimento(BigDecimal.TEN);
        movimentacao.setDataMovimento(LocalDate.now());
        movimentacao.setTipo(Tipo.D);
        movimentacao.setIsento(false);

        return movimentacao;
    }

    public MovimentacaoRequest mockMovimentacaoRequest(){
        MovimentacaoRequest movimentacaoRequest = new MovimentacaoRequest();
        movimentacaoRequest.setIdTarifa(1);
        movimentacaoRequest.setCpfCnpj("12345678910");
        movimentacaoRequest.setValor(BigDecimal.TEN);
        movimentacaoRequest.setData(LocalDate.parse("2020-09-14"));
        movimentacaoRequest.setTipo("D");

        return movimentacaoRequest;
    }

    public Customer mockCustomer(){
        Customer customer = new Customer();
        customer.setCpfCnpj("12345678910");

        return customer;
    }

    public Tarifa mockTarifa(){
        Tarifa tarifa = new Tarifa();
        tarifa.setId(1);

        return tarifa;
    }

}