package br.com.itau.tarifas.movimentacao.controller;

import br.com.itau.tarifas.movimentacao.DTO.MovimentacaoMapper;
import br.com.itau.tarifas.movimentacao.DTO.MovimentacaoRequest;
import br.com.itau.tarifas.movimentacao.models.Movimentacao;
import br.com.itau.tarifas.movimentacao.models.Tipo;
import br.com.itau.tarifas.movimentacao.service.MovimentacaoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = MovimentacaoController.class)
public class MovimentacaoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MovimentacaoService movimentacaoService;

    @MockBean
    private MovimentacaoMapper movimentacaoMapper;

    private Movimentacao movimentacao;

    private List<Movimentacao> movimentacaoList;

    @Before
    public void setUp() throws Exception {
        movimentacaoList = new ArrayList<>();
        movimentacaoList.add(mockMovimentacao());
    }

    @Test
    public void create() throws Exception {
        when(movimentacaoService.create(Mockito.any(Movimentacao.class))).thenReturn(mockMovimentacao());
        mockMvc.perform(post("/movimentacao/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(mockMovimentacaoRequest())))
                .andExpect(status().isCreated());
    }

    @Test
    public void getAll() throws Exception {
        when(movimentacaoService.getAll()).thenReturn(movimentacaoList);
        mockMvc.perform(get("/movimentacao/"))
                .andExpect(status().isOk());
    }

    @Test
    public void getById() throws Exception {
        when(movimentacaoService.getById(Mockito.anyInt())).thenReturn(mockMovimentacao());
        mockMvc.perform(get("/movimentacao/"+1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.idTarifa").value(1))
                .andExpect(jsonPath("$.valorMovimento").value(BigDecimal.TEN));
    }

    @Test
    public void pesquisarPorData() throws Exception {
        when(movimentacaoService.pesquisarData(Mockito.anyInt(), Mockito.anyInt())).thenReturn(movimentacaoList);
        mockMvc.perform(get("/movimentacao/mensal/"+Mockito.anyInt()+"/"+Mockito.anyInt()))
                .andExpect(status().isOk());
    }

    @Test
    public void getByCpfcnpj() throws Exception {
        when(movimentacaoService.getByCpfcnpj(Mockito.anyString())).thenReturn(movimentacaoList);
        mockMvc.perform(get("/movimentacao/cliente/12345678910"))
                .andExpect(status().isOk());
    }

    public Movimentacao mockMovimentacao(){
        Movimentacao movimentacao = new Movimentacao();
        movimentacao.setId(1);
        movimentacao.setIdTarifa(1);
        movimentacao.setCpfCnpj("12345678910");
        movimentacao.setValorMovimento(BigDecimal.TEN);
        movimentacao.setDataMovimento(LocalDate.now());
        movimentacao.setTipo(Tipo.D);
        movimentacao.setIsento(false);

        return movimentacao;
    }

    public MovimentacaoRequest mockMovimentacaoRequest(){
        MovimentacaoRequest movimentacaoRequest = new MovimentacaoRequest();
        movimentacaoRequest.setIdTarifa(1);
        movimentacaoRequest.setCpfCnpj("12345678910");
        movimentacaoRequest.setValor(BigDecimal.TEN);
        movimentacaoRequest.setData(LocalDate.parse("2020-09-14"));
        movimentacaoRequest.setTipo("D");

        return movimentacaoRequest;
    }
}