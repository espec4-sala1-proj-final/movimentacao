package br.com.itau.tarifas.movimentacao.client.tarifa;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Tarifa não encontrada")
public class TarifaNotFoundException extends RuntimeException {
}
