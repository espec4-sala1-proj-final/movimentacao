package br.com.itau.tarifas.movimentacao.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDate;

public class MovimentacaoRequest {

    @NotNull(message = "O Id da tarifa é obrigatório")
    @JsonProperty("id_tarifa")
    private Integer idTarifa;

    @NotBlank
    @NotNull
    @JsonProperty("cpf_cnpj")
    @Size(min = 11, message = "O CPF/CNPJ deve ter mais que 11 caracteres")
    private String cpfCnpj;

    @DecimalMin(value = "0.0", inclusive = true)
    private BigDecimal valor;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate data;

    @NotNull(message = "Tipo não pode ser nulo. Preencha com D para débito ou C para crédito")
    @NotBlank(message = "Tipo não pode ser vazio. Preencha com D para débito ou C para crédito")
    @Enumerated(EnumType.STRING)
    private String tipo;

    public Integer getIdTarifa() {
        return idTarifa;
    }

    public void setIdTarifa(Integer idTarifa) {
        this.idTarifa = idTarifa;
    }

    public String getCpfCnpj() {
        return cpfCnpj;
    }

    public void setCpfCnpj(String cpfCnpj) {
        this.cpfCnpj = cpfCnpj;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
