package br.com.itau.tarifas.movimentacao.componente;

import br.com.itau.tarifas.movimentacao.models.Movimentacao;
import br.com.itau.tarifas.movimentacao.service.MovimentacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class MovimentoConsumer {

    @Autowired
    private MovimentacaoService movimentacaoService;

    @KafkaListener(topics = "spec4-gp1-movimento-tarifa", groupId = "LEGADO")
    public void receber(@Payload Movimentacao movimentacao) {
        System.out.println("Enviei o movimento TARIFA "+movimentacao.getIdTarifa()+" CNPJ/CPF: " + movimentacao.getCpfCnpj() + " VALOR R$: "+ movimentacao.getValorMovimento() + " ISENTO: "+ movimentacao.getIsento() + " TIPO: "+ movimentacao.getTipo()+ " DATA: "+ movimentacao.getDataMovimento());

        movimentacaoService.create(movimentacao);
    }
}