package br.com.itau.tarifas.movimentacao.client.customer;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="cliente", configuration = CustomerClientConfiguration.class)
public interface CustomerClient {

    @GetMapping("/clientes/documento/{cpfcnpj}")
    Customer getByCpfcnpj(@PathVariable String cpfcnpj);
}
