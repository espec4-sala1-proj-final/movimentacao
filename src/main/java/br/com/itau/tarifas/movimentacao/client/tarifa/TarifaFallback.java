package br.com.itau.tarifas.movimentacao.client.tarifa;

public class TarifaFallback implements TarifaClient {

    @Override
    public Tarifa getById(Integer id) {
        throw new TarifaBadGatewayException();
    }

    @Override
    public Iterable<Vigencia> consultarVigencias(int tarifaId) {
        throw new TarifaBadGatewayException();
    }
}
