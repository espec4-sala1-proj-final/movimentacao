package br.com.itau.tarifas.movimentacao.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
public class Movimentacao {

    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private Integer id;

    private Integer idTarifa;

    private String cpfCnpj;

    private BigDecimal valorMovimento;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate dataMovimento;

    @Enumerated(EnumType.STRING)
    private Tipo tipo;

    private Boolean isento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdTarifa() {
        return idTarifa;
    }

    public void setIdTarifa(Integer idTarifa) {
        this.idTarifa = idTarifa;
    }

    public String getCpfCnpj() {
        return cpfCnpj;
    }

    public void setCpfCnpj(String cpfCnpj) {
        this.cpfCnpj = cpfCnpj;
    }

    public BigDecimal getValorMovimento() {
        return valorMovimento;
    }

    public void setValorMovimento(BigDecimal valorMovimento) {
        this.valorMovimento = valorMovimento;
    }

    public LocalDate getDataMovimento() {
        return dataMovimento;
    }

    public void setDataMovimento(LocalDate dataMovimento) {
        this.dataMovimento = dataMovimento;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    public Boolean getIsento() {
        return isento;
    }

    public void setIsento(Boolean isento) {
        this.isento = isento;
    }

    public Boolean isVigenciaValida(LocalDate dataInicio, LocalDate dataFinal){
        return (this.getDataMovimento().isBefore(dataFinal) || this.getDataMovimento().equals(dataFinal))
                && (this.getDataMovimento().isAfter(dataInicio) || this.getDataMovimento().equals(dataInicio));
    }

    public Boolean isValorValido(double valorMinimo){
        return this.getValorMovimento().compareTo(BigDecimal.valueOf(valorMinimo)) > -1;
    }

    @Override
    public String toString() {
        return "Movimentacao{" +
                "id=" + id +
                ", idTarifa=" + idTarifa +
                ", cpfCnpj='" + cpfCnpj + '\'' +
                ", valorMovimento=" + valorMovimento +
                ", dataMovimento=" + dataMovimento +
                ", tipo=" + tipo +
                ", isento=" + isento +
                '}';
    }
}
