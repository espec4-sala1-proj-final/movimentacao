package br.com.itau.tarifas.movimentacao.client.customer;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_GATEWAY, reason = "Serviço de cliente indisponível")
public class CustomerBadRequestException extends RuntimeException {
}
