package br.com.itau.tarifas.movimentacao.client.customer;

public class CustomerFallback implements CustomerClient {

    @Override
    public Customer getByCpfcnpj(String cpfcnpj) {
        throw new CustomerBadRequestException();
    }
}
