package br.com.itau.tarifas.movimentacao.models;

public enum Tipo {
    D("Débito"),
    C("Crédito");

    private String descricao;

    Tipo(String descricao){
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

}
