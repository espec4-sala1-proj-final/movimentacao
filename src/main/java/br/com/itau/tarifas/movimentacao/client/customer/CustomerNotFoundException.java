package br.com.itau.tarifas.movimentacao.client.customer;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Cliente não encontrado")
public class CustomerNotFoundException extends RuntimeException {
}
