package br.com.itau.tarifas.movimentacao.client.customer;

import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;

public class CustomerClientConfiguration {
    @Bean
    public ErrorDecoder getCostumerClientDecoder(){
        return new CustomerClientDecoder();
    }

    @Bean
    public Feign.Builder builder(){
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallback(new CustomerFallback(), RetryableException.class)
                .build();

        return Resilience4jFeign.builder(decorators);
    }

}
