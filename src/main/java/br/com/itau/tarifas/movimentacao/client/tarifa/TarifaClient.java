package br.com.itau.tarifas.movimentacao.client.tarifa;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="tarifa", configuration = TarifaClientConfiguration.class)
public interface TarifaClient {

    @GetMapping("/tarifas/{id}")
    Tarifa getById(@PathVariable Integer id);

    @GetMapping("/tarifas/{id}/vigencias")
    Iterable<Vigencia> consultarVigencias(@PathVariable(name = "id") int tarifaId);
}
