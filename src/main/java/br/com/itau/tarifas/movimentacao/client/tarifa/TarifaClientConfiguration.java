package br.com.itau.tarifas.movimentacao.client.tarifa;

import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class TarifaClientConfiguration {

    @Bean
    public ErrorDecoder getTarifaClientDecoder(){
        return new TarifaClientDecoder();
    }

    @Bean
    public Feign.Builder builder(){
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallback(new TarifaFallback(), RetryableException.class)
                .build();

        return Resilience4jFeign.builder(decorators);
    }


}
