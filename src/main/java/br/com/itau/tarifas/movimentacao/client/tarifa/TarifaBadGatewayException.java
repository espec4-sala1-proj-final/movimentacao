package br.com.itau.tarifas.movimentacao.client.tarifa;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_GATEWAY, reason = "Serviço de tarifa indisponível")
public class TarifaBadGatewayException extends RuntimeException {
}
