package br.com.itau.tarifas.movimentacao.client.tarifa;

import feign.Response;
import feign.codec.ErrorDecoder;

public class TarifaClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404){
            throw new TarifaNotFoundException();
        }
        return errorDecoder.decode(s, response);
    }
}
