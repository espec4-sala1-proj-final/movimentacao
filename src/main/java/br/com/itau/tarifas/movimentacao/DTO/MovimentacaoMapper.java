package br.com.itau.tarifas.movimentacao.DTO;

import br.com.itau.tarifas.movimentacao.models.Movimentacao;
import br.com.itau.tarifas.movimentacao.models.Tipo;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component
public class MovimentacaoMapper {
    public MovimentacaoResponse toMovimentacaoResponse(Movimentacao movimentacao) {
        MovimentacaoResponse movimentacaoResponse = new MovimentacaoResponse();
        movimentacaoResponse.setId(movimentacao.getId());
        movimentacaoResponse.setCpfCnpj(movimentacao.getCpfCnpj());
        movimentacaoResponse.setDataMovimento(movimentacao.getDataMovimento());
        movimentacaoResponse.setIdTarifa(movimentacao.getIdTarifa());
        movimentacaoResponse.setIsento(movimentacao.getIsento());
        movimentacaoResponse.setTipo(movimentacao.getTipo());
        movimentacaoResponse.setValorMovimento(movimentacao.getValorMovimento());
        return movimentacaoResponse;
    }

    public List<MovimentacaoResponse> toListMovimentacaoResponse (List<Movimentacao> listaMovimentacao){
        List<MovimentacaoResponse> listaResponse = new ArrayList<>();

        for(Movimentacao movimentacao : listaMovimentacao){
            listaResponse.add(this.toMovimentacaoResponse(movimentacao));
        }

        return listaResponse;
    }

    public Movimentacao toMovimentacao(MovimentacaoRequest movimentacaoRequest) {
        Movimentacao movimentacao = new Movimentacao();

        movimentacao.setCpfCnpj(movimentacaoRequest.getCpfCnpj());
        movimentacao.setDataMovimento(movimentacaoRequest.getData());
        movimentacao.setIdTarifa(movimentacaoRequest.getIdTarifa());
        movimentacao.setIsento(false);
        movimentacao.setTipo(Tipo.valueOf(movimentacaoRequest.getTipo().toUpperCase()));
        movimentacao.setValorMovimento(movimentacaoRequest.getValor());
        return movimentacao;
    }


}
