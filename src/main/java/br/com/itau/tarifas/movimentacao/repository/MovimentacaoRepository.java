package br.com.itau.tarifas.movimentacao.repository;

import br.com.itau.tarifas.movimentacao.models.Movimentacao;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.List;

public interface MovimentacaoRepository extends CrudRepository<Movimentacao, Integer> {

      List<Movimentacao> findByDataMovimentoBetween(LocalDate localDateInicio, LocalDate localDateFim);
      List<Movimentacao> findByCpfCnpj(String cpfCnpj);

    
}


