package br.com.itau.tarifas.movimentacao.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Movimentação inválida, data de vigência inválida")
public class MovimentacaoDataVigenciaInvalidaException extends RuntimeException{
}
