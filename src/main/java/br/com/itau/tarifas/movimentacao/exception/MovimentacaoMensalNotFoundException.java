package br.com.itau.tarifas.movimentacao.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Não foi localizado movimentação no mês selecionado")
public class MovimentacaoMensalNotFoundException extends RuntimeException{
}
