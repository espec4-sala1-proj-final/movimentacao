package br.com.itau.tarifas.movimentacao.controller;

import br.com.itau.tarifas.movimentacao.DTO.MovimentacaoMapper;
import br.com.itau.tarifas.movimentacao.DTO.MovimentacaoRequest;
import br.com.itau.tarifas.movimentacao.DTO.MovimentacaoResponse;
import br.com.itau.tarifas.movimentacao.models.Movimentacao;
import br.com.itau.tarifas.movimentacao.service.MovimentacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/movimentacao")
public class MovimentacaoController {

    @Autowired
    private MovimentacaoService movimentacaoService;

    @Autowired
    private MovimentacaoMapper movimentacaoMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Movimentacao create(@Valid @RequestBody MovimentacaoRequest createMovimentacaoRequest){
        Movimentacao movimentacao = movimentacaoMapper.toMovimentacao(createMovimentacaoRequest);
        return movimentacaoService.create(movimentacao);
    }

    @GetMapping
    public Iterable<Movimentacao> getAll(){
        return movimentacaoService.getAll();
    }

    @GetMapping("/{id}")
    public Movimentacao getById(@PathVariable Integer id){
        return movimentacaoService.getById(id);
    }


    @GetMapping("/mensal/{mes}/{ano}")
    public List<Movimentacao> pesquisarPorData(@PathVariable(name = "mes") int mes, @PathVariable(name = "ano") int ano){
            List <Movimentacao> movimentacao = movimentacaoService.pesquisarData(mes, ano);
            return movimentacao;
    }

    @GetMapping("/cliente/{cpfcnpj}")
    public List<MovimentacaoResponse> getByCpfcnpj(@PathVariable String cpfcnpj){
        List<Movimentacao> listaMovimentacaoDB = movimentacaoService.getByCpfcnpj(cpfcnpj);

        return movimentacaoMapper.toListMovimentacaoResponse(listaMovimentacaoDB);
    }

}
