package br.com.itau.tarifas.movimentacao.service;

import br.com.itau.tarifas.movimentacao.exception.MovimentacaoMensalNotFoundException;
import br.com.itau.tarifas.movimentacao.client.customer.Customer;
import br.com.itau.tarifas.movimentacao.client.customer.CustomerClient;
import br.com.itau.tarifas.movimentacao.client.tarifa.Tarifa;
import br.com.itau.tarifas.movimentacao.client.tarifa.TarifaClient;
import br.com.itau.tarifas.movimentacao.client.tarifa.Vigencia;
import br.com.itau.tarifas.movimentacao.exception.MovimentacaoDataVigenciaInvalidaException;
import br.com.itau.tarifas.movimentacao.exception.MovimentacaoNotFoundException;
import br.com.itau.tarifas.movimentacao.exception.MovimentacaoValorDeVigenciaInvalidoException;
import br.com.itau.tarifas.movimentacao.models.Movimentacao;
import br.com.itau.tarifas.movimentacao.repository.MovimentacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

@Service
public class MovimentacaoService {

    @Autowired
    private MovimentacaoRepository movimentacaoRepository;

    @Autowired
    private CustomerClient customerClient;

    @Autowired
    private TarifaClient tarifaClient;

    public Movimentacao create(Movimentacao movimentacao){
        Integer statusCode = 200;
        Customer customer = customerClient.getByCpfcnpj(movimentacao.getCpfCnpj());
        Tarifa tarifa = tarifaClient.getById(movimentacao.getIdTarifa());

        Iterable<Vigencia> vigencias = tarifaClient.consultarVigencias(tarifa.getId());
        Optional<Vigencia> vigenciaValida = StreamSupport.stream(vigencias.spliterator(), false)
                .filter(v -> movimentacao.isVigenciaValida(v.getInicioVigencia(), v.getFimVigencia())).findFirst();

        if(movimentacao.getValorMovimento().compareTo(BigDecimal.valueOf(0.00)) == 0){
            movimentacao.setIsento(true);
        }

        Movimentacao movimentacaoIncluida = movimentacaoRepository.save(movimentacao);

        if(!vigenciaValida.isPresent()){
            System.out.println("Movimentação incluída com data de vigência inválida! id " + movimentacaoIncluida.getId() );
            statusCode = 202;
        }

        if(!movimentacao.getIsento() && vigenciaValida.isPresent()
                && !movimentacao.isValorValido(vigenciaValida.get().getValorMinimo())){
            System.out.println("Movimentação incluída com valor inválido para cliente não isento! id " + movimentacaoIncluida.getId());
            statusCode = 202;
        }

        System.out.println("Movimentação incluída com sucesso! id " + movimentacaoIncluida.getId() + " no valor de " + movimentacaoIncluida.getValorMovimento() + " para cliente " + movimentacaoIncluida.getCpfCnpj());
        System.out.println(movimentacaoIncluida + " - status: " + statusCode);
        return movimentacaoIncluida;
    }

    public Iterable<Movimentacao> getAll(){
        return movimentacaoRepository.findAll();
    }

    public List<Movimentacao> getByCpfcnpj(String cpfCnpj){
        System.out.println("Acessando API de Cliente : " + cpfCnpj);
        Customer customer = customerClient.getByCpfcnpj(cpfCnpj);
        System.out.println("Consulta na base de Movimentacao por CPF/CNPJ :" + cpfCnpj);
        List<Movimentacao> listamovimentacao = movimentacaoRepository.findByCpfCnpj(customer.getCpfCnpj());

        if (listamovimentacao.isEmpty()){
            System.out.println("Não localizada o CPF/CNPJ na base : " + customer.getCpfCnpj());
            throw new MovimentacaoNotFoundException();
        }

        return listamovimentacao;
    }

    public Movimentacao getById(Integer id){
        Optional<Movimentacao> byId = movimentacaoRepository.findById(id);

        if(!byId.isPresent()){
            System.out.println("Movimentação não encontrada para o id: " + id);
            throw new MovimentacaoNotFoundException();
        }

        return byId.get();
    }

    public List<Movimentacao> pesquisarData(Integer mes, Integer ano){
        LocalDate localDateInicio = LocalDate.of(ano, mes, 01);
        LocalDate localDateFim = LocalDate.of(ano, mes, localDateInicio.lengthOfMonth());
        System.out.println("Busca os movimentos por mês: " + mes + " /ano: " + ano);
        try {
            return movimentacaoRepository.findByDataMovimentoBetween(localDateInicio, localDateFim);
        } catch (RuntimeException e){
            System.out.println("Não localizado registro no mês: " + mes + " /ano: " + ano);
            throw new MovimentacaoMensalNotFoundException();
        }

    }

}
