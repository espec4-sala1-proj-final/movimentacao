package br.com.itau.tarifas.movimentacao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class MovimentacaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovimentacaoApplication.class, args);
	}

}
