package br.com.itau.tarifas.movimentacao.client.customer;

import feign.Response;
import feign.codec.ErrorDecoder;

public class CustomerClientDecoder implements ErrorDecoder{
    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404){
            throw new CustomerNotFoundException();
        }
        return errorDecoder.decode(s, response);
    }

}
